package com.tes.echelonsdk;

public class Test {


    public static void main(String[] args) {

        String hex = "F0D10900520000004C003A00A2";
        byte[] arrayOfByte = hexStringToByteArray(hex);
        for (int i = 0; i < arrayOfByte.length; i++) {
            System.out.println("i=" + i + "====value" + arrayOfByte[i]);
        }

        int timestamp = arrayOfByte[4] << 0 & 0xFF | arrayOfByte[3] << 8 & 0xFF00;
//        int count=hexByte[8] << 0 & 0xFF | hexByte[5]<<24 & 0xFF000000 | hexByte[6]<<16 & 0xFF0000 | hexByte[7] <<8 & 0xFF00;
        final int count = arrayOfByte[5] << 24 & 0xFF000000 | arrayOfByte[6] << 16 & 0xFF0000 | arrayOfByte[7] << 8 & 0xFF00 | arrayOfByte[8] << 0 & 0xFF;
        int rpm=arrayOfByte[9]<<8 & 0xFF00 | arrayOfByte[10]<<0 & 0xFF;
        System.out.println("========"+timestamp+"========="+count+"=========="+rpm);



    }


    public static byte[] hexStringToByteArray(String paramString) {
        int i = paramString.length();
        byte[] arrayOfByte = new byte[i / 2];
        for (byte b = 0; b < i; b += 2)
            arrayOfByte[b / 2] = (byte) (byte) ((Character.digit(paramString.charAt(b), 16) << 4) + Character.digit(paramString.charAt(b + 1), 16));
        return arrayOfByte;
    }
}
