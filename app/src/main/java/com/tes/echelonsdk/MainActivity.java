package com.tes.echelonsdk;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.ImageFormat;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.tes.echelonsdk.santoble.BLEManager;
import com.tes.echelonsdk.santoble.BLEManagerListener;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.util.AppUtil;
import com.tes.echelonsdk.util.SharedPreUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Calendar;

import io.nlopez.smartlocation.SmartLocation;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends AppCompatActivity implements BLEManagerListener {

    public static String ManufacturerString = "mManufacturer=FTDI";
    final String ModelString1 = "mModel=FTDIUARTDemo";
    public static String ModelString2 = "mModel=Android Accessory FT312D";
    public static String VersionString = "mVersion=1.0";

    boolean bAutoConnect = false;

    private ListView listview;

    String mDeviceMAC = null;

    private LayoutInflater mInflater;
    private Button start;
    private TextView sum;
    private TextView success;
    private TextView fail;
    private EditText time;
    private EditText startTime;

    BaseAdapter mListviewAdapter = new BaseAdapter() {
        public int getCount() {
            return BLEManager.getInstance().getDiscoveredPeripherals().size();
        }

        public Object getItem(int param1Int) {
            return BLEManager.getInstance().getDiscoveredPeripherals().get(param1Int);
        }

        public long getItemId(int param1Int) {
            return 0L;
        }

        public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
            DeviceItem deviceItem;
            if (param1View == null) {
                param1View = MainActivity.this.mInflater.inflate(R.layout.layout_device_list_item, null);
                deviceItem = new DeviceItem((TextView) param1View.findViewById(R.id.textview));
                param1View.setTag(deviceItem);
            } else {
                deviceItem = (DeviceItem) param1View.getTag();
            }
            BLEPeripheral bLEPeripheral = BLEManager.getInstance().getDiscoveredPeripherals().get(param1Int);
            deviceItem.textview.setText(bLEPeripheral.getName());
            return param1View;
        }

        class DeviceItem {
            TextView textview;

            public DeviceItem(TextView textView) {
                this.textview = textView;
            }
        }
    };

    private void cancelBLE() {
        BLEManager.getInstance().stopScanPeripherals();
        BLEManager.getInstance().unregisterListener(this);
    }

    private boolean hasUSBAccessory() {
        UsbAccessory usbAccessory;
        UsbAccessory[] arrayOfUsbAccessory = ((UsbManager) getSystemService(USB_SERVICE)).getAccessoryList();
        if (arrayOfUsbAccessory == null) {
            usbAccessory = null;
        } else {
            usbAccessory = arrayOfUsbAccessory[0];
        }
        if (usbAccessory != null) {
            if (-1 == usbAccessory.toString().indexOf(ManufacturerString))
                return false;
            if (-1 == usbAccessory.toString().indexOf(VersionString))
                return false;
            if (usbAccessory.toString().indexOf(ModelString1) > 0 || usbAccessory.toString().indexOf(ModelString2) > 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    private void refreshListView() {
        runOnUiThread(new Runnable() {
            public void run() {
                MainActivity.this.mListviewAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void bleDidConnectPeripheral(BLEPeripheral paramBLEPeripheral) {
        BLEManager.getInstance().stopScanPeripherals();
        getSharedPreferences(MainActivity.class.getName(), 0).edit().putString("AUTO_CONNECT", paramBLEPeripheral.getAddress()).commit();
        startActivity(new Intent(this, ControlPaneActivity.class));
    }

    @Override
    public void bleDidDisconnectPeripheral(BLEPeripheral paramBLEPeripheral) {
        BLEManager.getInstance().stopScanPeripherals();
        refreshListView();
    }

    @Override
    public void bleDidDiscoverPeripheral(BLEPeripheral paramBLEPeripheral) {
        if (this.bAutoConnect && this.mDeviceMAC != null && paramBLEPeripheral != null && paramBLEPeripheral.getAddress() != null && paramBLEPeripheral.getAddress().equals(this.mDeviceMAC)) {
            BLEManager.getInstance().connectPeripheral(paramBLEPeripheral);
        } else {
            refreshListView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void bleDidInitialized() {
        BLEManager.getInstance().scanPeripherals();
        refreshListView();
    }


    @NeedsPermission({"android.permission.ACCESS_COARSE_LOCATION", "android.permission.WRITE_EXTERNAL_STORAGE"})
    public void gpsSettingsRequest() {
        if (!SmartLocation.with(this).location().state().locationServicesEnabled()) {
            runOnUiThread(new Runnable() {
                public void run() {
                    (new AlertDialog.Builder((Context) MainActivity.this)).setTitle("Enable Location Service").setMessage("Some smartphones need Location Service enabled to scan BLE correctly. Do you want to enable Location Service now?").setNegativeButton("No, thanks.", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface param2DialogInterface, int param2Int) {
                            MainActivityPermissionsDispatcher.initBLEWithPermissionCheck(MainActivity.this);
                        }
                    }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface param2DialogInterface, int param2Int) {
                            Intent intent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                            MainActivity.this.startActivity(intent);
                        }
                    }).show();
                }
            });
        } else {
            MainActivityPermissionsDispatcher.initBLEWithPermissionCheck(this);
        }
    }

    @NeedsPermission({"android.permission.BLUETOOTH"})
    protected void initBLE() {
        if (!((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter().isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            BLEManager.getInstance().registerListener(this);
            BLEManager.getInstance().initialize((Context) this);
        }
    }

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_main);
        if (!AppUtil.isMTB()) {
            finish();
        }
        this.listview = (ListView) findViewById(R.id.listview);
        sum = findViewById(R.id.sum);
        success = findViewById(R.id.success);
        fail = findViewById(R.id.fail);
        time = findViewById(R.id.time);
        startTime = findViewById(R.id.start_time);
        setTitle("ECHELON SDK");
        this.mInflater = getLayoutInflater();
        this.listview.setAdapter((ListAdapter) this.mListviewAdapter);
        this.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> param1AdapterView, View param1View, int param1Int, long param1Long) {
                BLEManager.getInstance().connectPeripheral((BLEPeripheral) MainActivity.this.mListviewAdapter.getItem(param1Int));
                finish();
            }
        });
        initTest();
    }


    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.menu_scan, paramMenu);
        return true;
    }


    private void initTest() {
        start = findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/bt");
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    File file1 = new File(file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".txt");
                    if (file1.exists()) {
                        file1.delete();
                    }
                    SharedPreUtils.getInstance(MainActivity.this).putString(AppUtil.LOG_PATH, file1.getAbsolutePath());
                    PrintStream out = new PrintStream(file1.getAbsolutePath());
                    System.setOut(out);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                SharedPreUtils.getInstance(MainActivity.this).putBoolean(AppUtil.IS_TEST, true);
                SharedPreUtils.getInstance(MainActivity.this).putString(AppUtil.TIME, time.getText().toString().trim());
                SharedPreUtils.getInstance(MainActivity.this).putString(AppUtil.START_TIME, startTime.getText().toString().trim());

                startConnect();
            }
        });
        Button cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreUtils.getInstance(MainActivity.this).putBoolean(AppUtil.IS_TEST, false);
                SharedPreUtils.getInstance(MainActivity.this).putInt(AppUtil.FAIL, 0);
                SharedPreUtils.getInstance(MainActivity.this).putInt(AppUtil.SUCCESS, 0);
            }
        });

        int successTime = SharedPreUtils.getInstance(this).getInt(AppUtil.SUCCESS, 0);
        int failTime = SharedPreUtils.getInstance(this).getInt(AppUtil.FAIL, 0);
        int sumTimes = successTime + failTime;
        sum.setText("Test Total: " + sumTimes + " times");
        success.setText("Success: " + successTime + " times");
        fail.setText("Fail: " + failTime + " times");


    }



    public void startConnect() {
        try {
            String path = SharedPreUtils.getInstance(MainActivity.this).getString(AppUtil.LOG_PATH);
            FileOutputStream outputStream = new FileOutputStream(path, true);
            PrintStream out = new PrintStream(outputStream);
            System.setOut(out);
        } catch (Exception e) {

        }
        try {
            listview.performItemClick(listview.getChildAt(0), 0, listview.getChildAt(0).getId());
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            BLEManager.getInstance().stopScanPeripherals();
            MainActivityPermissionsDispatcher.gpsSettingsRequestWithPermissionCheck(this);
            refreshListView();
            listview.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startConnect();
                }
            },5000);

        }

    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        int i = paramMenuItem.getItemId();
        if (i == R.id.action_scan) {
            BLEManager.getInstance().stopScanPeripherals();
            MainActivityPermissionsDispatcher.gpsSettingsRequestWithPermissionCheck(this);
            refreshListView();
            return true;
        }
        if (i == R.id.action_auto) {
            this.bAutoConnect = !bAutoConnect;
            if (this.bAutoConnect) {
                paramMenuItem.setTitle("Manual");
                this.mDeviceMAC = getSharedPreferences(MainActivity.class.getName(), 0).getString("AUTO_CONNECT", null);
            } else {
                paramMenuItem.setTitle("Auto");
                this.mDeviceMAC = null;
            }
            return true;
        }
        return super.onOptionsItemSelected(paramMenuItem);
    }

    protected void onPause() {
        if (BLEManager.getInstance().getInitialized())
            cancelBLE();
        super.onPause();
    }

    public boolean onPrepareOptionsMenu(Menu paramMenu) {
        return super.onPrepareOptionsMenu(paramMenu);
    }

    public void onRequestPermissionsResult(int paramInt, @NonNull String[] paramArrayOfString, @NonNull int[] paramArrayOfint) {
        super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfint);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, paramInt, paramArrayOfint);
    }

    protected void onResume() {
        super.onResume();
        if (hasUSBAccessory()) {
            startActivity(new Intent(this, ControlPaneActivity.class));
        } else {
            refreshListView();
            MainActivityPermissionsDispatcher.gpsSettingsRequestWithPermissionCheck(this);
        }

        start.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPreUtils.getInstance(MainActivity.this).getBoolean(AppUtil.IS_TEST, false)) {
                    startConnect();
                }
            }
        }, 15000);


    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santo/MainActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */