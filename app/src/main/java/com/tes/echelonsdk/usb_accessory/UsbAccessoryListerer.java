package com.tes.echelonsdk.usb_accessory;


import com.tes.echelonsdk.santoble.BLEPeripheralListener;

public interface UsbAccessoryListerer extends BLEPeripheralListener {
    void onReadErrorOccur();
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/usb_accessory/UsbAccessoryListerer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */