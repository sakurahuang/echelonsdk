package com.tes.echelonsdk.handler.treadmill;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.handler.Command;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;
import com.tes.echelonsdk.santoble.TreadmillPeripheral;
import com.tes.echelonsdk.santoble.TreadmillPeripheralListener;

public class TRUserKeyPressedNotify extends Command {
    protected byte getActionCode() {
        return -43;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int key = toInteger(arrayOfByte[3]);
        final int value = toInteger(arrayOfByte[4]);
        TreadmillPeripheral treadmillPeripheral = paramBLEPeripheral.getAsTreadmillPeripheral();
        if (treadmillPeripheral != null)
            treadmillPeripheral.replyNotify(this, arrayOfByte);
        if (listener != null && listener instanceof TreadmillPeripheralListener)
            Command.post(new Runnable() {
                final TreadmillPeripheralListener trListener = (TreadmillPeripheralListener) listener;

                public void run() {
                    this.trListener.userKeyPressed(key, value);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRUserKeyPressedNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */