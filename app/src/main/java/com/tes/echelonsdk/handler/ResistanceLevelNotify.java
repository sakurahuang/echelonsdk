package com.tes.echelonsdk.handler;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;
import com.tes.echelonsdk.santoble.Workout;

public class ResistanceLevelNotify extends Command {
  protected byte getActionCode() {
    return -46;
  }

  @Override
  public void handleReceivedData(ADData paramADData, BLEPeripheral blePeripheral,  final BLEPeripheralListener listener) {
    byte[] arrayOfByte = paramADData.bytes();
      System.out.println("=======ResistanceLevelNotify="+ADConverter.byteArrayToHexString(arrayOfByte));
    ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[] { ADConverter.byteArrayToHexString(arrayOfByte) });
    final int level = toInteger(arrayOfByte[3]);
    Workout.sharedInstance().setResistanceLevel(level);
    if (listener != null)
      Command.post(new Runnable() {
            public void run() {
              listener.resistanceLevelChanged(level);
            }
          }); 
  }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/ResistanceLevelNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */