package com.tes.echelonsdk.handler.treadmill;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.handler.Command;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;
import com.tes.echelonsdk.santoble.TreadmillPeripheralListener;
import com.tes.echelonsdk.santoble.Workout;

public class TRGetLimitsCmd extends Command {
    protected byte getActionCode() {
        return -93;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener trListener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int max_incline = toInteger(arrayOfByte[3]);
        final int min_incline = toInteger(arrayOfByte[4]);
        int m = toInteger(arrayOfByte[5], arrayOfByte[6]);
        int k = toInteger(arrayOfByte[7], arrayOfByte[8]);
        int j = k;
        final int countdown_seconds = m;
        int i = 0;
        if (!Workout.sharedInstance().isProtocolUnitMetric()) {
            i = (int) (m * 1.609344D);
            j = (int) (k * 1.609344D);
        }
        final double max_speed_in_kmh = i / 1000.0D;
        final double min_speed_in_kmh = j / 1000.0D;
        i = toInteger(arrayOfByte[9]);
        Workout.sharedInstance().setInclineRange(min_incline, max_incline);
        Workout.sharedInstance().setSpeedRange(min_speed_in_kmh, max_speed_in_kmh);
        Workout.sharedInstance().setCountdownSeconds(i);
        if (trListener != null && trListener instanceof TreadmillPeripheralListener)
            Command.post(new Runnable() {
                public void run() {
                    ((TreadmillPeripheralListener) trListener).onGetLimitsResponse(max_incline, min_incline, max_speed_in_kmh, min_speed_in_kmh, countdown_seconds);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRGetLimitsCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */