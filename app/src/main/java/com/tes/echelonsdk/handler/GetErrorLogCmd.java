package com.tes.echelonsdk.handler;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;

import java.util.Arrays;

public class GetErrorLogCmd extends Command {
    protected byte getActionCode() {
        return -94;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        final byte[] errorCodes = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(errorCodes)});
       byte[]  arrayBytes = Arrays.copyOfRange(errorCodes, 3, errorCodes.length - 1);
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onGetErrorLogResponse(arrayBytes);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/GetErrorLogCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */