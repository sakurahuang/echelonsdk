package com.tes.echelonsdk.handler;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.santoble.BLEManager;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;
import com.tes.echelonsdk.santoble.Workout;

public class WorkoutStatusNotify extends Command {
    protected byte getActionCode() {
        return -47;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", ADConverter.byteArrayToHexString(arrayOfByte));
        if (toInteger(arrayOfByte[2]) >= 10) {
            if (shared_handler != null) {
                shared_handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (BLEManager.getInstance().getPeripheral() != null) {
                            BLEManager.getInstance().getPeripheral().getDeviceInfo();
                        }
                    }
                });
                return;
            }
        }
        byte b2 = arrayOfByte[3];
        int timestamp = arrayOfByte[4] << 0 & 0xFF;
        int count = arrayOfByte[5] << 24 & 0xFF000000 | arrayOfByte[6] << 16 & 0xFF0000 | arrayOfByte[7] << 8 & 0xFF00 | arrayOfByte[8] << 0 & 0xFF;
        final int rpm = arrayOfByte[9] << 8 & 0xFF00 | arrayOfByte[10] << 0 & 0xFF;
        Workout.setRpmRatio(rpm);
        byte hr = arrayOfByte[11];
        double speed = Workout.getSpeedMultiplier();
        int time = (timestamp - 4) < 0 ? 0 : timestamp - 4;
        double distance = Workout.getDistancePerCount() * time / 60;
        final double watt = Workout.sharedInstance().getWatt(rpm);
        if (Workout.sharedInstance().getPreviousCount() > count)
            Workout.sharedInstance().resetWorkout();
        Workout.sharedInstance().addCaloriesWithCurrentCount(count);
        final double calories = Workout.sharedInstance().getCalories();
        int finalTime = timestamp - 1 < 0 ? 0 : timestamp - 1;
        int finalCount = count - 1 < 0 ? 0 : count - 1;
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.workoutStatusChanged(finalTime, finalCount, rpm, hr, speed, distance, calories, watt);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/WorkoutStatusNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */