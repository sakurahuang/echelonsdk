package com.tes.echelonsdk.handler;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;
import com.tes.echelonsdk.santoble.Workout;
import com.tes.echelonsdk.usb_accessory.UsbAccessoryManager;


public class GetDeviceInfoCmd extends Command {
    protected byte getActionCode() {
        return -95;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int modelID = toInteger(arrayOfByte[3]);
        final String hwVer = String.format("%02d.%02d", new Object[]{Integer.valueOf(toInteger(arrayOfByte[4])), Integer.valueOf(toInteger(arrayOfByte[5]))});
        final String fwVer = String.format("%02d.%02d.%02d", new Object[]{Integer.valueOf(toInteger(arrayOfByte[6])), Integer.valueOf(toInteger(arrayOfByte[7])), Integer.valueOf(toInteger(arrayOfByte[8]))});
        if (paramBLEPeripheral != null) {
            if (modelID >= 16 && modelID < 32) {
                paramBLEPeripheral.setAsRowerMode();
            } else if (modelID >= 32) {
                paramBLEPeripheral.setAsTreadmillMode();
            }
        } else if (UsbAccessoryManager.isInitialized()) {
            if (modelID >= 16 && modelID < 32) {
                UsbAccessoryManager.getInstance().setAsRowerMode();
            } else if (modelID >= 32) {
                throw new IllegalArgumentException("Has No Treadmill Support!");
            }
        }
        Workout.sharedInstance().setModelID(modelID);
        Workout.sharedInstance().setFwVer(toInteger(arrayOfByte[6]), toInteger(arrayOfByte[7]), toInteger(arrayOfByte[8]));
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onGetDeviceInfoResponse(modelID, hwVer, fwVer);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/GetDeviceInfoCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */