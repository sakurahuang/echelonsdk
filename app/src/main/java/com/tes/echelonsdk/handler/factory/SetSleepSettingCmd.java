package com.tes.echelonsdk.handler.factory;


import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.handler.Command;

public class SetSleepSettingCmd extends Command {
    int mResistanceLevel = 0;

    public SetSleepSettingCmd(int paramInt) {
        this.mResistanceLevel = paramInt;
    }

    public ADData compactRequestData() {
        this.commandData.appendByte((byte) this.mResistanceLevel);
        return super.compactRequestData();
    }

    protected byte getActionCode() {
        return 112;
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/factory/SetSleepSettingCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */