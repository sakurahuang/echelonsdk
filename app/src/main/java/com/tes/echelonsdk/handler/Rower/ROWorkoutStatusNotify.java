package com.tes.echelonsdk.handler.Rower;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.handler.Command;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;
import com.tes.echelonsdk.santoble.Workout;

public class ROWorkoutStatusNotify extends Command {
    protected byte getActionCode() {
        return -47;
    }

    public void handleReceivedData(ADData paramADData, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        byte b11 = arrayOfByte[3];
        int timestamp = arrayOfByte[4] << 0 & 0xFF;
        Workout.sharedInstance().setTimestamp(timestamp);
        byte direction = arrayOfByte[5];
        final int count = arrayOfByte[6] << 24 & 0xFF000000 | arrayOfByte[7] << 16 & 0xFF0000 | arrayOfByte[8] << 8 & 0xFF00 | arrayOfByte[9] << 0 & 0xFF;
        final int rpm = arrayOfByte[10] << 8 & 0xFF00 | arrayOfByte[11] << 0 & 0xFF;
        Workout.setRpmRatio(rpm);
        byte hr = arrayOfByte[12];
        double speed = Workout.getSpeedMultiplier();
        final double distance_in_km = (arrayOfByte[15] << 16 & 0xFF0000 | arrayOfByte[16] << 8 & 0xFF00 | arrayOfByte[17] << 0 & 0xFF) / 1000.0D;

        if (Workout.sharedInstance().getPreviousCount() > count)
            Workout.sharedInstance().resetWorkout();

        final double watt = Workout.sharedInstance().getWatt(rpm);

        Workout.sharedInstance().addCaloriesWithCurrentCount(count);
        double calories = Workout.sharedInstance().getCalories();

        int time500 = Workout.sharedInstance().getTimestamp() / 500;
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.workoutStatusChanged(timestamp, direction, count, rpm, hr, speed, distance_in_km, time500, calories, watt);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/Rower/ROWorkoutStatusNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */