package com.tes.echelonsdk.handler;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;
import com.tes.echelonsdk.santoble.Workout;

public class GetResistanceLevelRangeCmd extends Command {
    protected byte getActionCode() {
        return -93;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener listener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        final int max = toInteger(arrayOfByte[3]);
        final int min = toInteger(arrayOfByte[4]);
        Workout.sharedInstance().setResistanceLevelRange(max, min);
        if (listener != null)
            Command.post(new Runnable() {
                public void run() {
                    listener.onGetResistanceLevelRangeResponse(max, min);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/dex2jar-2.1/dex-tools-2.1-SNAPSHOT/classes-dex2jar.jar!/changyow/santoble/handler/GetResistanceLevelRangeCmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */