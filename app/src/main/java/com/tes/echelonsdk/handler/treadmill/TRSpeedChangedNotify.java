package com.tes.echelonsdk.handler.treadmill;

import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.handler.Command;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.BLEPeripheralListener;
import com.tes.echelonsdk.santoble.TreadmillPeripheral;
import com.tes.echelonsdk.santoble.TreadmillPeripheralListener;
import com.tes.echelonsdk.santoble.Workout;

public class TRSpeedChangedNotify extends Command {
    protected byte getActionCode() {
        return -45;
    }

    public void handleReceivedData(ADData paramADData, BLEPeripheral paramBLEPeripheral, final BLEPeripheralListener trListener) {
        byte[] arrayOfByte = paramADData.bytes();
        ADLog.i(getClass().getName(), "handleReceivedData %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        int j = toInteger(arrayOfByte[3], arrayOfByte[4]);
        int i = j;
        if (!Workout.sharedInstance().isProtocolUnitMetric())
            i = (int) (j * 1.609344D);
        final double speed_in_kmh = i / 1000.0D;
        Workout.sharedInstance().setSpeed(speed_in_kmh);
        TreadmillPeripheral treadmillPeripheral = paramBLEPeripheral.getAsTreadmillPeripheral();
        if (treadmillPeripheral != null)
            treadmillPeripheral.replyNotify(this, arrayOfByte);
        if (trListener != null && trListener instanceof TreadmillPeripheralListener)
            Command.post(new Runnable() {
                public void run() {
                    ((TreadmillPeripheralListener) trListener).speedChanged(speed_in_kmh);
                }
            });
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/handler/treadmill/TRSpeedChangedNotify.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */