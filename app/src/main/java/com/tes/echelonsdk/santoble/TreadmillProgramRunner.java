package com.tes.echelonsdk.santoble;

import com.tes.echelonsdk.handler.Command;
import com.tes.echelonsdk.handler.treadmill.TRSetInclineCmd;
import com.tes.echelonsdk.handler.treadmill.TRSetSpeedCmd;
import com.tes.echelonsdk.util.TreadmillProgramSet;

import java.util.Iterator;
import java.util.List;

public class TreadmillProgramRunner implements Runnable {
    private void setProgramIncline(int paramInt) {
        TRSetInclineCmd tRSetInclineCmd = new TRSetInclineCmd(Workout.sharedInstance().availableIncline(paramInt));
        if (BLEManager.getInstance().getPeripheral() != null)
            BLEManager.getInstance().getPeripheral().writeValue((Command) tRSetInclineCmd);
    }

    private void setProgramSpeed(double paramDouble) {
        TRSetSpeedCmd tRSetSpeedCmd = new TRSetSpeedCmd(Workout.sharedInstance().availableSpeed(paramDouble));
        if (BLEManager.getInstance().getPeripheral() != null)
            BLEManager.getInstance().getPeripheral().writeValue((Command) tRSetSpeedCmd);
    }

    private void stopProgramWorktou() {
        if (BLEManager.getInstance().getPeripheral() != null)
            BLEManager.getInstance().getPeripheral().stopWorkout();
    }

    public void run() {
        int j;
        TreadmillProgramSet treadmillProgramSet = null;
        if (Workout.sharedInstance().getTreadmillProgramSets() == null)
            stopProgramWorktou();
        int k = Workout.sharedInstance().getTimestamp();
        List<TreadmillProgramSet> list = Workout.sharedInstance().getTreadmillProgramSets();
        int i = 0;
        List list1 = null;
        Iterator<TreadmillProgramSet> iterator = list.iterator();
        while (true) {
            j = i;
            list = list1;
            if (iterator.hasNext()) {
                treadmillProgramSet = iterator.next();
                j = i + treadmillProgramSet.getDurationSeconds();
                i = j;
                if (k < j)
                    break;
                continue;
            }
            break;
        }
        if (treadmillProgramSet != null && k < j - treadmillProgramSet.getDurationSeconds() + 3) {
            setProgramIncline(treadmillProgramSet.getTargetIncline());
            setProgramSpeed(treadmillProgramSet.getTargetSpeed());
        } else if (treadmillProgramSet == null) {
            stopProgramWorktou();
        }
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/TreadmillProgramRunner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */