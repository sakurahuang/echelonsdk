package com.tes.echelonsdk.santoble;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.os.Handler;

import androidx.annotation.NonNull;

import com.tes.echelonsdk.adble.ble.ADBlePeripheral;
import com.tes.echelonsdk.adble.ble.ADBlePeripheralCallback;
import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.adble.utility.ADData;
import com.tes.echelonsdk.adble.utility.ADLog;
import com.tes.echelonsdk.handler.AckCmd;
import com.tes.echelonsdk.handler.Command;
import com.tes.echelonsdk.handler.GetDeviceInfoCmd;
import com.tes.echelonsdk.handler.GetErrorLogCmd;
import com.tes.echelonsdk.handler.GetFTMPValidDateCmd;
import com.tes.echelonsdk.handler.GetResistanceLevelCmd;
import com.tes.echelonsdk.handler.GetResistanceLevelRangeCmd;
import com.tes.echelonsdk.handler.GetWorkoutControlStateCmd;
import com.tes.echelonsdk.handler.ResistanceLevelNotify;
import com.tes.echelonsdk.handler.Rower.ROWorkoutStatusNotify;
import com.tes.echelonsdk.handler.SetResistanceLevelCmd;
import com.tes.echelonsdk.handler.SetWorkoutControlStateCmd;
import com.tes.echelonsdk.handler.SyncTimeCmd;
import com.tes.echelonsdk.handler.WorkoutControlStateNotify;
import com.tes.echelonsdk.handler.WorkoutStatusNotify;
import com.tes.echelonsdk.handler.factory.GetSleepSettingCmd;
import com.tes.echelonsdk.handler.factory.SetSleepSettingCmd;
import com.tes.echelonsdk.handler.treadmill.TRGetDeviceInfoCmd;
import com.tes.echelonsdk.handler.treadmill.TRGetFanSpeedLevelCmd;
import com.tes.echelonsdk.handler.treadmill.TRGetFanSpeedLimitCmd;
import com.tes.echelonsdk.handler.treadmill.TRGetInclineCmd;
import com.tes.echelonsdk.handler.treadmill.TRGetLimitsCmd;
import com.tes.echelonsdk.handler.treadmill.TRGetSpeedCmd;
import com.tes.echelonsdk.handler.treadmill.TRInclineChangedNotify;
import com.tes.echelonsdk.handler.treadmill.TRSpeedChangedNotify;
import com.tes.echelonsdk.handler.treadmill.TRUserKeyPressedNotify;
import com.tes.echelonsdk.handler.treadmill.TRWorkoutControlStateNotify;
import com.tes.echelonsdk.handler.treadmill.TRWorkoutStatusNotify;
import com.tes.echelonsdk.util.ChecksumUtil;

import java.io.EOFException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import okio.Buffer;

public class BLEPeripheral implements ADBlePeripheralCallback {
    private static final long SEND_COMMAND_INTERVAL = 2000L;

    private static final String TAG = "BLEPeripheral";

    private static final UUID mNotifyCharacteristicUuid;

    private static final UUID mReadCharacteristicUuid;

    private static final UUID mServiceUuid = UUID.fromString("0bf669f1-45f2-11e7-9598-0800200c9a66");

    private static final UUID mWriteCharacteristicUuid = UUID.fromString("0bf669f2-45f2-11e7-9598-0800200c9a66");

    private boolean bInitialized = false;

    private boolean bLogging = false;

    private boolean bNotifyLock = false;

    private boolean bReadLock = false;

    long lastHandledCommandTimestamp = Long.MAX_VALUE;

    private CopyOnWriteArrayList<Command> mCommands = new CopyOnWriteArrayList<Command>();

    private Handler mHandler = null;

    private BLEPeripheralListener mListener = null;

    private final Buffer mNotifyBuffer = new Buffer();

    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private ADBlePeripheral mPeripheral;

    private final Buffer mReadBuffer = new Buffer();

    private BluetoothGattCharacteristic mReadCharacteristic;

    Runnable mSendCmdRunnable = new Runnable() {
        public void run() {
            BLEPeripheral.this.mHandler.removeCallbacks(BLEPeripheral.this.mSendCmdRunnable);
            if (System.currentTimeMillis() - BLEPeripheral.this.lastHandledCommandTimestamp >= BLEPeripheral.this.maxWaitingTime) {
                BLEPeripheral.this.mHandler.post(new Runnable() {
                    public void run() {
                        BLEManager.getInstance().cancelPeripheralConnection();
                        BLEManager.getInstance().forceNotifyBleDidDisconnect();
                    }
                });
                BLEPeripheral.this.mHandler = null;
                return;
            }
//            if (BLEPeripheral.this.mPeripheral != null)
//                BLEPeripheral.this.mHandler.postDelayed(BLEPeripheral.this.mSendCmdRunnable, 2000L);
            try {
                BLEPeripheral.this.ack();
            } catch (Exception exception) {
                BLEPeripheral.this.mHandler.removeCallbacks(BLEPeripheral.this.mSendCmdRunnable);
                BLEPeripheral.this.mHandler = null;
            }
        }
    };

    private BluetoothGattCharacteristic mWriteCharacteristic;

    long maxWaitingTime = 5000L;

    public StringBuffer rcvLog = new StringBuffer();

    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");

    static {
        mReadCharacteristicUuid = UUID.fromString("0bf669f3-45f2-11e7-9598-0800200c9a66");
        mNotifyCharacteristicUuid = UUID.fromString("0bf669f4-45f2-11e7-9598-0800200c9a66");
    }

    BLEPeripheral(@NonNull ADBlePeripheral paramADBlePeripheral) {
        reset();
        this.mPeripheral = paramADBlePeripheral;
        this.mPeripheral.setCallback(this);
        this.mWriteCharacteristic = null;
        this.mReadCharacteristic = null;
        this.mNotifyCharacteristic = null;
        this.mHandler = SharedHandlerThread.createHandler();
    }

    private void appendFactoryCommands(CopyOnWriteArrayList<Command> paramCopyOnWriteArrayList) {
        paramCopyOnWriteArrayList.add(new GetSleepSettingCmd());
    }

    private void insertLog(String paramString) {
        if (this.bLogging)
            this.rcvLog.insert(0, paramString);
    }

    private void processCommand(ADData paramADData) {
        for (Command command : this.mCommands) {
            if (command.isExpectedData(paramADData, false)) {
                command.handleReceivedData(paramADData, this, this.mListener);
                this.lastHandledCommandTimestamp = System.currentTimeMillis();
            }
        }
    }

    private void processNotifyData() {
        synchronized (this.mNotifyBuffer) {
            if (!this.bNotifyLock) {
                boolean bool;
                if (!this.bNotifyLock) {
                    bool = true;
                } else {
                    bool = false;
                }
                this.bNotifyLock = bool;
                seekStarter(this.mNotifyBuffer);
                long l = this.mNotifyBuffer.size();
                while (l >= 4L) {
                    long l1 = (this.mNotifyBuffer.getByte(2L) + 4);
                    if (l < l1)
                        break;
//                    this.mNotifyBuffer.copyTo(mNotifyBuffer, 0L, l1);
                    ADData aDData = new ADData(mNotifyBuffer.readByteArray());
                    if (ChecksumUtil.verifyChecksum(aDData.bytes())) {
                        processCommand(aDData);
                        try {
                            this.mNotifyBuffer.skip(l1);
                        } catch (EOFException eOFException) {
                        }
                    } else {
                        try {
                            this.mNotifyBuffer.skip(1L);
                        } catch (EOFException eOFException) {
                        }
                    }
                    seekStarter(this.mNotifyBuffer);
                    l = this.mNotifyBuffer.size();
                    mNotifyBuffer.clear();
                }
                synchronized (this.mNotifyBuffer) {
                    this.bNotifyLock = false;
                    return;
                }
            }
            return;
        }
    }

    private void processReadData() {
        synchronized (this.mReadBuffer) {
            if (!this.bReadLock) {
                boolean bool;
                if (!this.bReadLock) {
                    bool = true;
                } else {
                    bool = false;
                }
                this.bReadLock = bool;
                seekStarter(this.mReadBuffer);
                long l = this.mReadBuffer.size();
                while (l >= 4L) {
                    long l1 = (this.mReadBuffer.getByte(2L) + 4);
                    if (l < l1)
                        break;
//                    this.mReadBuffer.copyTo(mReadBuffer, 0L, l1);
                    ADData aDData = new ADData(mReadBuffer.readByteArray());
                    if (ChecksumUtil.verifyChecksum(aDData.bytes())) {
                        processCommand(aDData);
                        try {
                            this.mReadBuffer.skip(l1);
                        } catch (EOFException eOFException) {
                        }
                    } else {
                        try {
                            this.mReadBuffer.skip(1L);
                        } catch (EOFException eOFException) {
                        }
                    }
                    seekStarter(this.mReadBuffer);
                    l = this.mReadBuffer.size();
                    mReadBuffer.clear();
                }
                synchronized (this.mReadBuffer) {
                    this.bReadLock = false;
                    return;
                }
            }
            return;
        }
    }

    private void seekStarter(Buffer paramBuffer) {
        // Byte code:
        //   0: aload_1
        //   1: invokevirtual size : ()J
        //   4: lstore #4
        //   6: lconst_0
        //   7: lstore_2
        //   8: lload_2
        //   9: lload #4
        //   11: lcmp
        //   12: ifge -> 35
        //   15: bipush #-16
        //   17: aload_1
        //   18: lload_2
        //   19: invokevirtual getByte : (J)B
        //   22: if_icmpne -> 28
        //   25: goto -> 39
        //   28: lload_2
        //   29: lconst_1
        //   30: ladd
        //   31: lstore_2
        //   32: goto -> 8
        //   35: ldc2_w -1
        //   38: lstore_2
        //   39: lload_2
        //   40: ldc2_w -1
        //   43: lcmp
        //   44: ifne -> 54
        //   47: aload_1
        //   48: invokevirtual clear : ()V
        //   51: goto -> 65
        //   54: lload_2
        //   55: lconst_0
        //   56: lcmp
        //   57: ifle -> 65
        //   60: aload_1
        //   61: lload_2
        //   62: invokevirtual skip : (J)V
        //   65: return
        //   66: astore_1
        //   67: goto -> 65
        // Exception table:
        //   from	to	target	type
        //   60	65	66	java/io/IOException
    }

    public void ack() {
        writeValue((Command) new AckCmd());
    }

    public String getAddress() {
        return this.mPeripheral.getAddress();
    }

    public TreadmillPeripheral getAsTreadmillPeripheral() {
        return Workout.sharedInstance().isTreadmill() ? new TreadmillPeripheral(this) : null;
    }

    public void getDeviceInfo() {
        writeValue((Command) new GetDeviceInfoCmd());
    }

    public void getErrorLog() {
        writeValue((Command) new GetErrorLogCmd());
    }

    public void getFTMPValidDate() {
        writeValue((Command) new GetFTMPValidDateCmd());
    }

    public String getName() {
        ADBlePeripheral aDBlePeripheral = this.mPeripheral;
        return (aDBlePeripheral != null && aDBlePeripheral.getName() != null) ? this.mPeripheral.getName() : "Unnamed";
    }

    ADBlePeripheral getPeripheral() {
        return this.mPeripheral;
    }

    public int getPeripheralState() {
        ADBlePeripheral aDBlePeripheral = this.mPeripheral;
        return (aDBlePeripheral != null) ? aDBlePeripheral.getBleConnectionStatus() : 0;
    }

    public void getResistanceLevel() {
        writeValue((Command) new GetResistanceLevelCmd());
    }

    public void getResistanceLevelRange() {
        writeValue((Command) new GetResistanceLevelRangeCmd());
    }

    public void getSleepSetting() {
        writeValue((Command) new GetSleepSettingCmd());
    }

    public void getWorkoutControlState() {
        writeValue((Command) new GetWorkoutControlStateCmd());
    }

    public boolean isConnected() {
        boolean bool;
        if (2 == this.mPeripheral.getBleConnectionStatus()) {
            bool = true;
        } else {
            bool = false;
        }
        return bool;
    }

    public boolean isInitialized() {
        return this.bInitialized;
    }

    public void peripheralDidDiscoverServices(ADBlePeripheral paramADBlePeripheral, BluetoothGattService[] paramArrayOfBluetoothGattService, int paramInt) {
        if (paramInt != 0) {
            paramADBlePeripheral.discoverServices();
            return;
        }
        int i = paramArrayOfBluetoothGattService.length;
        byte b = 0;
        for (paramInt = b; b < i; paramInt++) {
            BluetoothGattService bluetoothGattService = paramArrayOfBluetoothGattService[b];
            int j = paramInt;
            if (bluetoothGattService.getUuid().equals(mServiceUuid)) {
                Iterator<BluetoothGattCharacteristic> iterator = bluetoothGattService.getCharacteristics().iterator();
                while (true) {
                    j = paramInt;
                    if (iterator.hasNext()) {
                        BluetoothGattCharacteristic bluetoothGattCharacteristic = iterator.next();
                        UUID uUID = bluetoothGattCharacteristic.getUuid();
                        j = bluetoothGattCharacteristic.getProperties();
                        if (uUID.equals(mNotifyCharacteristicUuid) && (j & 0x10) > 0) {
                            ADLog.i("BLEPeripheral", "%s found NotifyCharacteristic", new Object[]{paramADBlePeripheral.getName()});
                            this.mNotifyCharacteristic = bluetoothGattCharacteristic;
                        } else if (uUID.equals(mWriteCharacteristicUuid) && (j & 0x4) > 0) {
                            ADLog.i("BLEPeripheral", "%s found WriteCharacteristic", new Object[]{paramADBlePeripheral.getName()});
                            this.mWriteCharacteristic = bluetoothGattCharacteristic;
                        } else if (uUID.equals(mReadCharacteristicUuid) && (j & 0x10) > 0) {
                            ADLog.i("BLEPeripheral", "%s found ReadCharacteristic", new Object[]{paramADBlePeripheral.getName()});
                            this.mReadCharacteristic = bluetoothGattCharacteristic;
                        } else {
                            continue;
                        }
                        paramInt++;
                        continue;
                    }
                    break;
                }
            }
            b++;
        }
        if (paramInt == 9 && this.mNotifyCharacteristic != null) {
            ADLog.i("BLEPeripheral", "%s setNotifyValueForCharacteristic", new Object[]{paramADBlePeripheral.getName()});
            paramADBlePeripheral.setNotifyValueForCharacteristic(this.mNotifyCharacteristic, true);
            paramADBlePeripheral.setNotifyValueForCharacteristic(this.mReadCharacteristic, true);
            if (this.mListener != null) {
                getDeviceInfo();
            }
        }
    }

    public void peripheralDidReadRSSI(ADBlePeripheral paramADBlePeripheral, int paramInt1, int paramInt2) {
    }

    public void peripheralDidReadValueForCharacteristic(ADBlePeripheral paramADBlePeripheral, BluetoothGattCharacteristic paramBluetoothGattCharacteristic, int paramInt) {
    }

    public void peripheralDidUpdateValueForCharacteristic(ADBlePeripheral paramADBlePeripheral, BluetoothGattCharacteristic paramBluetoothGattCharacteristic, byte[] paramArrayOfbyte) {
        if (!this.bInitialized) {
            this.bInitialized = true;
            if (this.mListener != null) {
                try {
                    if (BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral() != null) {
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().getLimits();
                    } else {
                        BLEManager.getInstance().getPeripheral().getResistanceLevelRange();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                this.mListener.peripheralInitialized();
                this.mHandler.postDelayed(this.mSendCmdRunnable, 2000L);
            }
        }
        byte[] arrayOfByte = paramBluetoothGattCharacteristic.getValue();
        ADLog.i("BLEPeripheral", "RCV(Notify) %s", new Object[]{ADConverter.byteArrayToHexString(arrayOfByte)});
        insertLog(String.format("%s: %s\n", new Object[]{this.sdf.format(new Date()), ADConverter.byteArrayToHexString(arrayOfByte)}));
        if (arrayOfByte != null && arrayOfByte.length > 0) {
            if (paramBluetoothGattCharacteristic.getService().getUuid().equals(mServiceUuid) && paramBluetoothGattCharacteristic.getUuid().equals(mNotifyCharacteristicUuid)) {
                this.mNotifyBuffer.clear();
                this.mNotifyBuffer.write(arrayOfByte);
                processNotifyData();
            }
            if (paramBluetoothGattCharacteristic.getService().getUuid().equals(mServiceUuid) && paramBluetoothGattCharacteristic.getUuid().equals(mReadCharacteristicUuid)) {
                this.mReadBuffer.clear();
                this.mReadBuffer.write(arrayOfByte);
                processReadData();
            }
        }
    }

    public void peripheralDidWriteValueForCharacteristic(ADBlePeripheral paramADBlePeripheral, BluetoothGattCharacteristic paramBluetoothGattCharacteristic, int paramInt) {
    }

    public void peripheralDidWriteValueForDescriptor(ADBlePeripheral paramADBlePeripheral, BluetoothGattDescriptor paramBluetoothGattDescriptor, int paramInt) {
    }

    void reset() {
        synchronized (mNotifyCharacteristicUuid) {
            this.mReadBuffer.clear();
            this.mNotifyBuffer.clear();
            this.mCommands.clear();
            appendFactoryCommands(this.mCommands);
            AckCmd ackCmd = new AckCmd();
            mCommands.add(ackCmd);
            GetDeviceInfoCmd getDeviceInfoCmd = new GetDeviceInfoCmd();
            mCommands.add(getDeviceInfoCmd);
            GetErrorLogCmd getErrorLogCmd = new GetErrorLogCmd();
            mCommands.add(getErrorLogCmd);
            GetResistanceLevelRangeCmd getResistanceLevelRangeCmd = new GetResistanceLevelRangeCmd();
            mCommands.add(getResistanceLevelRangeCmd);
            GetWorkoutControlStateCmd getWorkoutControlStateCmd = new GetWorkoutControlStateCmd();
            mCommands.add(getWorkoutControlStateCmd);
            GetResistanceLevelCmd getResistanceLevelCmd = new GetResistanceLevelCmd();
            mCommands.add(getResistanceLevelCmd);
            WorkoutControlStateNotify workoutControlStateNotify = new WorkoutControlStateNotify();
            mCommands.add(workoutControlStateNotify);
            WorkoutStatusNotify workoutStatusNotify = new WorkoutStatusNotify();
            mCommands.add(workoutStatusNotify);
            ResistanceLevelNotify resistanceLevelNotify = new ResistanceLevelNotify();
            mCommands.add(resistanceLevelNotify);
            GetFTMPValidDateCmd getFTMPValidDateCmd=new GetFTMPValidDateCmd();
            mCommands.add(getFTMPValidDateCmd);
        }
    }

    public void setAsRowerMode() {
        synchronized (mNotifyCharacteristicUuid) {
            this.mCommands.clear();
            appendFactoryCommands(this.mCommands);
            AckCmd ackCmd = new AckCmd();
            mCommands.add(ackCmd);
            GetDeviceInfoCmd getDeviceInfoCmd = new GetDeviceInfoCmd();
            mCommands.add(getDeviceInfoCmd);
            GetErrorLogCmd getErrorLogCmd = new GetErrorLogCmd();
            mCommands.add(getErrorLogCmd);
            GetResistanceLevelRangeCmd getResistanceLevelRangeCmd = new GetResistanceLevelRangeCmd();
            mCommands.add(getResistanceLevelRangeCmd);
            GetWorkoutControlStateCmd getWorkoutControlStateCmd = new GetWorkoutControlStateCmd();
            mCommands.add(getWorkoutControlStateCmd);
            GetResistanceLevelCmd getResistanceLevelCmd = new GetResistanceLevelCmd();
            mCommands.add(getResistanceLevelCmd);
            WorkoutControlStateNotify workoutControlStateNotify = new WorkoutControlStateNotify();
            mCommands.add(workoutControlStateNotify);
            ROWorkoutStatusNotify rOWorkoutStatusNotify = new ROWorkoutStatusNotify();
            mCommands.add(rOWorkoutStatusNotify);
            ResistanceLevelNotify resistanceLevelNotify = new ResistanceLevelNotify();
            mCommands.add(resistanceLevelNotify);
        }
    }

    public void setAsTreadmillMode() {
        synchronized (mNotifyCharacteristicUuid) {
            this.mCommands.clear();
            appendFactoryCommands(this.mCommands);
            AckCmd ackCmd = new AckCmd();
            mCommands.add(ackCmd);
            TRGetDeviceInfoCmd tRGetDeviceInfoCmd = new TRGetDeviceInfoCmd();
            mCommands.add(tRGetDeviceInfoCmd);
            GetErrorLogCmd getErrorLogCmd = new GetErrorLogCmd();
            mCommands.add(getErrorLogCmd);
            TRGetLimitsCmd tRGetLimitsCmd = new TRGetLimitsCmd();
            mCommands.add(tRGetLimitsCmd);
            GetWorkoutControlStateCmd getWorkoutControlStateCmd = new GetWorkoutControlStateCmd();
            mCommands.add(getWorkoutControlStateCmd);
            TRGetInclineCmd tRGetInclineCmd = new TRGetInclineCmd();
            mCommands.add(tRGetInclineCmd);
            TRGetSpeedCmd tRGetSpeedCmd = new TRGetSpeedCmd();
            mCommands.add(tRGetSpeedCmd);
            TRGetFanSpeedLimitCmd tRGetFanSpeedLimitCmd = new TRGetFanSpeedLimitCmd();
            mCommands.add(tRGetFanSpeedLimitCmd);
            TRGetFanSpeedLevelCmd tRGetFanSpeedLevelCmd = new TRGetFanSpeedLevelCmd();
            mCommands.add(tRGetFanSpeedLevelCmd);
            TRWorkoutControlStateNotify tRWorkoutControlStateNotify = new TRWorkoutControlStateNotify();
            mCommands.add(tRWorkoutControlStateNotify);
            TRWorkoutStatusNotify tRWorkoutStatusNotify = new TRWorkoutStatusNotify();
            mCommands.add(tRWorkoutStatusNotify);
            TRInclineChangedNotify tRInclineChangedNotify = new TRInclineChangedNotify();
            mCommands.add(tRInclineChangedNotify);
            TRSpeedChangedNotify tRSpeedChangedNotify = new TRSpeedChangedNotify();
            mCommands.add(tRSpeedChangedNotify);
            TRUserKeyPressedNotify tRUserKeyPressedNotify = new TRUserKeyPressedNotify();
            mCommands.add(tRUserKeyPressedNotify);
        }
    }

    public void setListener(BLEPeripheralListener paramBLEPeripheralListener) {
        this.mListener = paramBLEPeripheralListener;
    }

    public void setLogging(boolean paramBoolean) {
        this.bLogging = paramBoolean;
    }

    public void setResistanceLevel(int paramInt) {
        writeValue((Command) new SetResistanceLevelCmd(Workout.sharedInstance().availableResistanceLevel(paramInt)));
    }

    public void setSleepSetting(int paramInt) {
        if (paramInt == 0) {
            paramInt = 0;
        } else {
            paramInt = 1;
        }
        writeValue((Command) new SetSleepSettingCmd(paramInt));
    }

    public void setWorkoutControlState(int paramInt) {
        writeValue((Command) new SetWorkoutControlStateCmd(paramInt));

    }

    public void startWorkout() {
        setWorkoutControlState(1);
    }

    public void stopWorkout() {
        setWorkoutControlState(0);
        Workout.sharedInstance().resetWorkout();
    }

    public void syncTime(Date paramDate1, Date paramDate2) {
        if (paramDate1 != null && paramDate2 != null)
            writeValue((Command) new SyncTimeCmd(paramDate1, paramDate2));
    }

    void writeValue(Command paramCommand) {
        if (this.mPeripheral != null && this.mWriteCharacteristic != null && isConnected()) {
            byte[] arrayOfByte = paramCommand.compactRequestData().bytes();
            if (paramCommand instanceof GetFTMPValidDateCmd)
                insertLog(String.format("%s: GetFTMPValid %s\n", new Object[]{this.sdf.format(new Date()), ADConverter.byteArrayToHexString(arrayOfByte)}));
            if (paramCommand instanceof SyncTimeCmd)
                insertLog(String.format("%s: SyncTime %s\n", new Object[]{this.sdf.format(new Date()), ADConverter.byteArrayToHexString(arrayOfByte)}));
            this.mPeripheral.writeValueForCharacteristic(this.mWriteCharacteristic, arrayOfByte, null);
        }
    }

    void writeValue(byte[] paramArrayOfbyte) {
        try {
            if (this.mPeripheral != null && this.mWriteCharacteristic != null && isConnected() && paramArrayOfbyte != null && paramArrayOfbyte.length > 4)
                this.mPeripheral.writeValueForCharacteristic(this.mWriteCharacteristic, paramArrayOfbyte, null);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/BLEPeripheral.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */