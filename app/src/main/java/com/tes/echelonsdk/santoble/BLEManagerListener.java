package com.tes.echelonsdk.santoble;

public interface BLEManagerListener {
  void bleDidConnectPeripheral(BLEPeripheral paramBLEPeripheral);
  
  void bleDidDisconnectPeripheral(BLEPeripheral paramBLEPeripheral);
  
  void bleDidDiscoverPeripheral(BLEPeripheral paramBLEPeripheral);
  
  void bleDidInitialized();
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/BLEManagerListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */