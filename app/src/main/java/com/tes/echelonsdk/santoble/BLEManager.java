package com.tes.echelonsdk.santoble;

import android.bluetooth.le.ScanRecord;
import android.content.Context;
import android.os.ParcelUuid;

import androidx.annotation.NonNull;

import com.tes.echelonsdk.adble.ble.ADBleCentralManager;
import com.tes.echelonsdk.adble.ble.ADBleCentralManagerCallback;
import com.tes.echelonsdk.adble.ble.ADBlePeripheral;
import com.tes.echelonsdk.adble.utility.ADLog;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class BLEManager implements ADBleCentralManagerCallback {
    private static final String BLE_DEVICE_UUID = "0bf669f0-45f2-11e7-9598-0800200c9a66";

    private static String TAG = "BLEManager";

    private static BLEManager mInstance;

    @Override
    public void bleCentralManagerDidConnectPeripheral(ADBlePeripheral paramADBlePeripheral) {
        BLEPeripheral bLEPeripheral = (BLEPeripheral) BLEManager.this.mPeripherals.get(paramADBlePeripheral.getAddress());
        if (bLEPeripheral != null) {
            bLEPeripheral.getPeripheral().discoverServices();
            for (BLEManagerListener bLEManagerListener : BLEManager.this.mListeners) {
                try {
                    bLEManagerListener.bleDidConnectPeripheral(bLEPeripheral);
                } catch (RuntimeException runtimeException) {
                    ADLog.e(BLEManager.TAG, "Unexpected exception in bleCentralManagerDidConnectPeripheral", new Object[0]);
                    BLEManager.this.unregisterListener(bLEManagerListener);
                }
            }
        }
    }

    @Override
    public void bleCentralManagerDidDisconnectPeripheral(ADBlePeripheral paramADBlePeripheral) {
        BLEPeripheral bLEPeripheral = (BLEPeripheral) BLEManager.this.mPeripherals.get(paramADBlePeripheral.getAddress());
        if (bLEPeripheral != null)
            for (BLEManagerListener bLEManagerListener : BLEManager.this.mListeners) {
                try {
                    bLEManagerListener.bleDidDisconnectPeripheral(bLEPeripheral);
                } catch (RuntimeException runtimeException) {
                    ADLog.e(BLEManager.TAG, "Unexpected exception in bleCentralManagerDidDisconnectPeripheral", new Object[0]);
                    BLEManager.this.unregisterListener(bLEManagerListener);
                }
            }
        if (BLEManager.this.mPeripheral != null && BLEManager.this.mPeripheral.equals(bLEPeripheral))
            BLEManager.this.mPeripheral = null;
    }

    @Override
    public void bleCentralManagerDidDiscover(ADBlePeripheral paramADBlePeripheral, int paramInt, ScanRecord paramScanRecord) {
        List<ParcelUuid> list = paramScanRecord.getServiceUuids();
        if (list != null && list.size() > 0) {
            ParcelUuid parcelUuid = null;
            paramScanRecord = null;
            for (ParcelUuid parcelUuid1 : list) {
                if (parcelUuid1.equals(ParcelUuid.fromString("0bf669f0-45f2-11e7-9598-0800200c9a66")))
                    parcelUuid = parcelUuid1;
            }
            if (parcelUuid != null) {
                BLEPeripheral bLEPeripheral2 = (BLEPeripheral) BLEManager.this.mPeripherals.get(paramADBlePeripheral.getAddress());
                BLEPeripheral bLEPeripheral1 = bLEPeripheral2;
                if (bLEPeripheral2 == null) {
                    bLEPeripheral1 = new BLEPeripheral(paramADBlePeripheral);
                    BLEManager.this.mPeripherals.put(paramADBlePeripheral.getAddress(), bLEPeripheral1);
                }
                if (!BLEManager.this.mScannedPeripherals.containsKey(paramADBlePeripheral.getAddress())) {
                    BLEManager.this.mScannedPeripherals.put(paramADBlePeripheral.getAddress(), bLEPeripheral1);
                    for (BLEManagerListener bLEManagerListener : BLEManager.this.mListeners) {
                        try {
                            bLEManagerListener.bleDidDiscoverPeripheral(bLEPeripheral1);
                        } catch (RuntimeException runtimeException) {
                            ADLog.e(BLEManager.TAG, "Unexpected exception in bleCentralManagerDidDiscover", new Object[0]);
                            BLEManager.this.unregisterListener(bLEManagerListener);
                        }
                    }
                }
            }
        }

    }

    @Override
    public void bleCentralManagerDidInitialized() {
        BLEManager.this.mInitialized = true;
        for (BLEManagerListener bLEManagerListener : BLEManager.this.mListeners) {
            try {
                bLEManagerListener.bleDidInitialized();
            } catch (RuntimeException runtimeException) {
                ADLog.e(BLEManager.TAG, "Unexpected exception in bleCentralManagerDidInitialized", new Object[0]);
                BLEManager.this.unregisterListener(bLEManagerListener);
            }
        }
    }

    private boolean mInitialized = false;

    private CopyOnWriteArrayList<BLEManagerListener> mListeners = null;

    private BLEPeripheral mPeripheral = null;

    private final ConcurrentHashMap<String, BLEPeripheral> mPeripherals;

    private final ConcurrentHashMap<String, BLEPeripheral> mScannedPeripherals;

    private boolean mToScan;

    protected BLEManager() {
        resetBleCallBack();
        this.mScannedPeripherals = new ConcurrentHashMap<String, BLEPeripheral>();
        this.mPeripherals = new ConcurrentHashMap<String, BLEPeripheral>();
        this.mListeners = new CopyOnWriteArrayList<BLEManagerListener>();
    }

    public static BLEManager getInstance() {
        if (mInstance == null)
            mInstance = new BLEManager();
        return mInstance;
    }

    private void resetBleCallBack() {
        ADBleCentralManager.getInstance().setCallBack(this);
    }

    public void cancelPeripheralConnection() {
        if (this.mPeripheral != null)
            try {
                ADBleCentralManager.getInstance().cancelPeripheralConnection(this.mPeripheral.getPeripheral());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        this.mPeripheral = null;
    }

    public void connectPeripheral(@NonNull BLEPeripheral paramBLEPeripheral) {
        if (this.mPeripheral == null && paramBLEPeripheral != null) {
            this.mPeripheral = paramBLEPeripheral;
            ADBleCentralManager.getInstance().connectPeripheral(paramBLEPeripheral.getPeripheral());
        }
    }

    public void forceNotifyBleDidDisconnect() {
        for (BLEManagerListener bLEManagerListener : this.mListeners) {
            try {
                bLEManagerListener.bleDidDisconnectPeripheral(null);
            } catch (RuntimeException runtimeException) {
                ADLog.e(TAG, "Unexpected exception in bleCentralManagerDidDisconnectPeripheral", new Object[0]);
                unregisterListener(bLEManagerListener);
            }
        }
        this.mPeripheral = null;
    }

    public ArrayList<BLEPeripheral> getDiscoveredPeripherals() {
        ConcurrentHashMap<String, BLEPeripheral> concurrentHashMap = this.mScannedPeripherals;
        return (concurrentHashMap != null) ? new ArrayList<BLEPeripheral>(concurrentHashMap.values()) : null;
    }

    public boolean getInitialized() {
        return this.mInitialized;
    }

    public BLEPeripheral getPeripheral() {
        return this.mPeripheral;
    }

    public void initialize(@NonNull Context paramContext) {
        if (paramContext != null) {
            ADBleCentralManager.getInstance().initialize(paramContext);
            return;
        }
        throw new IllegalArgumentException("applicationContext = null");
    }

    public void registerListener(@NonNull BLEManagerListener paramBLEManagerListener) {
        if (paramBLEManagerListener != null && !this.mListeners.contains(paramBLEManagerListener))
            this.mListeners.add(paramBLEManagerListener);
    }

    public void scanPeripherals() {
        this.mToScan = true;
        this.mScannedPeripherals.clear();
        this.mPeripherals.clear();
        ADBleCentralManager.getInstance().scan();
    }

    public void stopScanPeripherals() {
        ADBleCentralManager.getInstance().cancelScan();
        this.mToScan = false;
    }

    public void unregisterListener(@NonNull BLEManagerListener paramBLEManagerListener) {
        if (paramBLEManagerListener != null && this.mListeners.contains(paramBLEManagerListener))
            this.mListeners.remove(paramBLEManagerListener);
    }
}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santoble/BLEManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */