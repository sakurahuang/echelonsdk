package com.tes.echelonsdk;

import android.app.PendingIntent;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tes.echelonsdk.adapter.RecyclerAdapter;
import com.tes.echelonsdk.adble.utility.ADConverter;
import com.tes.echelonsdk.santoble.BLEManager;
import com.tes.echelonsdk.santoble.BLEManagerListener;
import com.tes.echelonsdk.santoble.BLEPeripheral;
import com.tes.echelonsdk.santoble.TreadmillPeripheralListener;
import com.tes.echelonsdk.santoble.Workout;
import com.tes.echelonsdk.usb_accessory.UsbAccessoryListerer;
import com.tes.echelonsdk.util.AppUtil;
import com.tes.echelonsdk.util.InactivityTimer;
import com.tes.echelonsdk.util.SharedPreUtils;
import com.tes.echelonsdk.util.TreadmillProgramSet;
import com.tes.echelonsdk.usb_accessory.UsbAccessoryManager;

import java.util.ArrayList;
import java.util.Date;

public class ControlPaneActivity extends AppCompatActivity implements BLEManagerListener, TreadmillPeripheralListener, UsbAccessoryListerer {
    private static final long SEND_COMMAND_INTERVAL = 1000L;

    static Handler mHandler = new Handler(Looper.getMainLooper());
    private static final String ACTION_USB_PERMISSION = "com.UARTLoopback.USB_PERMISSION";
    public static String ManufacturerString = "mManufacturer=FTDI";
    public static String ModelString1 = "mModel=FTDIUARTDemo";
    public static String ModelString2 = "mModel=Android Accessory FT312D";
    public static String VersionString = "mVersion=1.0";
    private UsbAccessory usbAccessory;
    private UsbAccessoryManager mUsbAccessoryManager = null;

    boolean asc = true;

    double calories = 0.0D;

    int consume = 0;

    int controlState = 0;

    int count = 0;

    int cycle = 0;

    int direction = 0;

    double distance = 0.0D;

    int hr = 0;

    int incline = 0;

    int level = 1;

    private RecyclerView listview;

    PendingIntent mPermissionIntent;

    boolean mPermissionRequestPending = false;
    private RecyclerAdapter adapter;

    private InactivityTimer timer;

    private Runnable mSendCmdRunnable = new Runnable() {
        public void run() {
            ControlPaneActivity.mHandler.removeCallbacks(ControlPaneActivity.this.mSendCmdRunnable);
            ControlPaneActivity.mHandler.postDelayed(ControlPaneActivity.this.mSendCmdRunnable, 1000L);
            ControlPaneActivity.this.timerCount++;
            if (ControlPaneActivity.this.consume > 0) {
                ControlPaneActivity.this.consume--;
            }
            if (ControlPaneActivity.this.timerCount % 3 == 0) {
                int i;
                if (ControlPaneActivity.this.asc) {
                    i = ControlPaneActivity.this.level + 1;
                } else {
                    i = ControlPaneActivity.this.level - 1;
                }
                if (i <= ControlPaneActivity.this.maxLevel && i > 0) {
                    try {
                        BLEManager.getInstance().getPeripheral().setResistanceLevel(i);
                    } catch (Exception exception) {
                    }
                } else {
                    ControlPaneActivity.this.consume = 30;
                    if (i == 0) {
                        ControlPaneActivity.this.asc = true;
                    } else {
                        ControlPaneActivity.this.asc = false;
                    }
                    if (i == 0)
                        if (ControlPaneActivity.this.maxLevel == 10) {
                            ControlPaneActivity.this.maxLevel = 32;
                        } else {
                            ControlPaneActivity.this.maxLevel = 10;
                            ControlPaneActivity.this.cycle++;
                        }
                }
            }
            System.gc();
        }
    };

    UsbAccessory mUsbAccessory;
    UsbManager mUsbManager;


    /***********USB broadcast receiver*******************************************/
    public BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbAccessory accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        Toast.makeText(ControlPaneActivity.this, "Allow USB Permission", Toast.LENGTH_SHORT).show();
                        mPermissionRequestPending = true;
                        mUsbAccessory = accessory;
                        mUsbAccessoryManager = UsbAccessoryManager.createInstance(mUsbManager, mUsbAccessory);
                        mUsbAccessoryManager.setListener(ControlPaneActivity.this);
                        mUsbAccessoryManager.openAccessory();
                        otgBikeMode();
                    } else {
                        Toast.makeText(ControlPaneActivity.this, "Deny USB Permission", Toast.LENGTH_SHORT).show();
                        Log.d("LED", "permission denied for accessory " + accessory);
                    }
                    mPermissionRequestPending = true;
                }
            } else if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
                if (mUsbAccessoryManager != null) {
                    mUsbAccessoryManager.destoryAccessory(true);
                }
            } else {
                Log.d("LED", "....");
            }
        }
    };

    int maxLevel = 10;

    int rpm = 0;

    double speed = 0.0D;

    Runnable startDiscover = new Runnable() {
        public void run() {
            ((BluetoothManager) ControlPaneActivity.this.getSystemService(BLUETOOTH_SERVICE)).getAdapter().startDiscovery();
            ControlPaneActivity.mHandler.postDelayed(ControlPaneActivity.this.stopDiscovery, 5000L);
        }
    };

    Runnable stopDiscovery = new Runnable() {
        public void run() {
            ((BluetoothManager) ControlPaneActivity.this.getSystemService(BLUETOOTH_SERVICE)).getAdapter().cancelDiscovery();
            ControlPaneActivity.mHandler.postDelayed(ControlPaneActivity.this.startDiscover, 20000L);
        }
    };

    int time = 0;

    int time500 = 0;

    int timerCount = 0;

    private TextView txvResponse;

    private TextView txvStatus;

    double watt = 0.0D;

    private void displayValues() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Looper.myLooper().toString());
        stringBuilder.append(Thread.currentThread().toString());
        Log.i("TAG", stringBuilder.toString());
        runOnUiThread(new Runnable() {
            public void run() {
                ControlPaneActivity.this.doDisplayValues();
            }
        });
    }

    private void doDisplayValues() {
        String str1;
        int i = this.controlState;
        if (i == 1) {
            str1 = "Start";
        } else if (i == 0) {
            str1 = "Stop";
        } else if (i == 2) {
            str1 = "Pause";
        } else {
            str1 = String.format("%x", new Object[]{Integer.valueOf(i)});
        }
        i = this.time;
        String str2 = String.format("%2d:%2d:%2d", new Object[]{Integer.valueOf(i / 3600), Integer.valueOf(i % 3600 / 60), Integer.valueOf(i % 60)});
        if (Workout.sharedInstance().isRower()) {
            str1 = String.format("Control State: %s\nResistance Level: %d\nTime: %s\t\tstroke: %d\nSPM: %03d\t\tHR: %03d\nSpeed: %02.02f\t\tDistance: %03.02f\nTime/500: %d:%d\t\tDirection:%d\nCalories: %04.01f\t\tWatt: %03.01f", new Object[]{
                    str1, Integer.valueOf(this.level), str2, Integer.valueOf(this.count), Integer.valueOf(this.rpm), Integer.valueOf(this.hr), Double.valueOf(this.speed), Double.valueOf(this.distance), Integer.valueOf(this.time500 / 60), Integer.valueOf(this.time500 % 60),
                    Integer.valueOf(this.direction), Double.valueOf(this.calories), Double.valueOf(this.watt)});
        } else if (Workout.sharedInstance().isTreadmill()) {
            str1 = String.format("Control State: %s\nIncline: %d\nTime: %s\t\tHR: %03d\nSpeed: %02.02f\t\tDistance: %03.02f\nCalories: %04.01f\t\tWatt: %03.01f", new Object[]{str1, Integer.valueOf(this.incline), str2, Integer.valueOf(this.hr), Double.valueOf(this.speed), Double.valueOf(this.distance), Double.valueOf(this.calories), Double.valueOf(this.watt)});
        } else {
            str1 = String.format("Control State: %s\nResistance Level: %d\nTime: %s\t\tcount: %d\nRPM: %03d\t\tHR: %03d\nSpeed: %02.02f\t\tDistance: %03.02f\nCalories: %04.01f\t\tWatt: %03.01f", new Object[]{str1, Integer.valueOf(this.level), str2, Integer.valueOf(this.count), Integer.valueOf(this.rpm), Integer.valueOf(this.hr), Double.valueOf(this.speed), Double.valueOf(this.distance), Double.valueOf(this.calories), Double.valueOf(this.watt)});
        }
        str2 = str1;
        if (this.timerCount > 0)
            str2 = String.format("%s\nCycle: %d, totalTime:%d:%d:%d", new Object[]{str1, Integer.valueOf(this.cycle), Integer.valueOf(this.timerCount / 3600), Integer.valueOf(this.timerCount % 3600 / 60), Integer.valueOf(this.timerCount % 60)});
        this.txvStatus.setText(str2);
    }

    private void otgBikeMode() {
        runOnUiThread(new Runnable() {
            public void run() {
                ArrayList<String> arrayList = new ArrayList();
                arrayList.add("get Device Info");
                arrayList.add("get Error Log");
                arrayList.add("get Resistance Level Range");
                arrayList.add("get Resistance Level");
                arrayList.add("get Workout Control State");
                arrayList.add("Stop Workout");
                arrayList.add("Start Workout");
                arrayList.add("Increase Resistance Level");
                arrayList.add("Decrease Resistance Level");
                arrayList.add("RPM Ratio 1");
                arrayList.add("RPM Ratio 4.4");
                arrayList.add("Get Sleep Setting");
                arrayList.add("Set Sleep Setting: off/0");
                arrayList.add("Set Sleep Setting: on/1");
                RecyclerAdapter adapter = new RecyclerAdapter(arrayList);
                adapter.setListener(new RecyclerAdapter.ItemClickListener() {
                    @Override
                    public void click(int position) {
                        if (mUsbAccessoryManager == null)
                            return;
                        switch (position) {
                            case 0://getDevices info
                                mUsbAccessoryManager.getDeviceInfo();
                                break;
                            case 1://getError
                                mUsbAccessoryManager.getErrorLog();
                                break;
                            case 2:
                                mUsbAccessoryManager.getResistanceLevelRange();
                                break;
                            case 3:
                                mUsbAccessoryManager.getResistanceLevel();
                                break;
                            case 4:
                                mUsbAccessoryManager.getWorkoutControlState();
                                break;
                            case 5:
                                mUsbAccessoryManager.stopWorkout();
                                break;
                            case 6:
                                mUsbAccessoryManager.startWorkout();
                                break;
                            case 7:
                                mUsbAccessoryManager.setResistanceLevel(level + 1);
                                break;
                            case 8:
                                mUsbAccessoryManager.setResistanceLevel(level - 1);
                                break;
                            case 9:
                                Workout.setRpmRatio(1.0d);
                                break;
                            case 10:
                                Workout.setRpmRatio(4.4d);
                                break;
                            case 11:
                                mUsbAccessoryManager.getSleepSetting();
                                break;
                            case 12:
                                mUsbAccessoryManager.setSleepSetting(0);
                                break;
                            case 13:
                                mUsbAccessoryManager.setSleepSetting(1);
                                break;
                        }
                    }
                });
                listview.setAdapter(adapter);
            }
        });
    }

    private void prepareTreadmillCommands() {
        ArrayList<String> arrayList = new ArrayList();
        arrayList.add("get Device Info");
        arrayList.add("get Error Log");
        arrayList.add("get Limits");
        arrayList.add("get Incline");
        arrayList.add("get Speed");
        arrayList.add("get Workout Control State");
        arrayList.add("Stop Workout");
        arrayList.add("Start Workout");
        if (this.controlState == 2) {
            arrayList.add("Pause Workout(will stop workout)");
        } else {
            arrayList.add("Pause Workout");
        }
        arrayList.add("get Fan Speed Limit");
        arrayList.add("get Fan Speed Level");
        arrayList.add("set Fan Speed Level 0");
        arrayList.add("set Fan Speed Level 1");
        arrayList.add("set Fan Speed Level 2");
        arrayList.add("set Fan Speed Level 3");
        arrayList.add("program tset");
        adapter = new RecyclerAdapter(arrayList);
        listview.setAdapter(adapter);
    }

    private void setResponseMessage(final String message) {
        if (message != null)
            runOnUiThread(new Runnable() {
                public void run() {
                    ControlPaneActivity.this.txvResponse.setText(message);
                }
            });
    }

    private void showControlPane() {
        runOnUiThread(new Runnable() {
            public void run() {
                ControlPaneActivity.this.listview.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showTreadmillCommands() {
        prepareTreadmillCommands();
        adapter.setListener(new RecyclerAdapter.ItemClickListener() {
            @Override
            public void click(int position) {
                ArrayList<TreadmillProgramSet> arrayList;
                switch (position) {
                    default:
                        break;
                    case 15:
                        if (ControlPaneActivity.this.controlState == 2) {
                            BLEManager.getInstance().getPeripheral().startWorkout();
                        } else if (ControlPaneActivity.this.controlState != 0) {
                            return;
                        }
                        arrayList = new ArrayList();
                        for (int param1Int = 0; param1Int < 13; param1Int++)
                            arrayList.add(new TreadmillProgramSet(10, param1Int, param1Int * 0.3D + 0.8D));
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().startWorkout(arrayList);
                        break;
                    case 14:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().setFanSpeedLevel(4);
                        break;
                    case 13:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().setFanSpeedLevel(2);
                        break;
                    case 12:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().setFanSpeedLevel(1);
                        break;
                    case 11:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().setFanSpeedLevel(0);
                        break;
                    case 10:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().getFanSpeedLevel();
                        break;
                    case 9:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().getFanSpeedLimit();
                        break;
                    case 8:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().pauseWorkout();
                        break;
                    case 7:
                        BLEManager.getInstance().getPeripheral().startWorkout();
                        break;
                    case 6:
                        BLEManager.getInstance().getPeripheral().stopWorkout();
                        break;
                    case 5:
                        BLEManager.getInstance().getPeripheral().getWorkoutControlState();
                        break;
                    case 4:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().getSpeed();
                        break;
                    case 3:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().getIncline();
                        break;
                    case 2:
                        BLEManager.getInstance().getPeripheral().getAsTreadmillPeripheral().getLimits();
                        break;
                    case 1:
                        BLEManager.getInstance().getPeripheral().getErrorLog();
                        break;
                    case 0:
                        BLEManager.getInstance().getPeripheral().getDeviceInfo();
                        break;
                }
            }
        });
    }

    private void startResistanceTest() {
        getWindow().addFlags(128);
        this.listview.setVisibility(4);
        BLEManager.getInstance().getPeripheral().startWorkout();
        mHandler.postDelayed(this.mSendCmdRunnable, 1000L);
    }

    public void bleDidConnectPeripheral(BLEPeripheral paramBLEPeripheral) {
    }

    public void bleDidDisconnectPeripheral(BLEPeripheral paramBLEPeripheral) {
        finish();
    }

    public void bleDidDiscoverPeripheral(BLEPeripheral paramBLEPeripheral) {
    }

    public void bleDidInitialized() {
    }

    public void controlStateChanged(int paramInt) {
        this.controlState = paramInt;
        displayValues();
        if (Workout.sharedInstance().isTreadmill())
            runOnUiThread(new Runnable() {
                public void run() {
                    ControlPaneActivity.this.prepareTreadmillCommands();
                }
            });
    }

    public void inclineChanged(int paramInt) {
        this.incline = paramInt;
        displayValues();
    }

    public void onAckResponse() {
    }

    public void onBackPressed() {
        BLEManager.getInstance().cancelPeripheralConnection();
        super.onBackPressed();
    }

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_control_pane);
        setTitle("ECHELONSDK");
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread param1Thread, Throwable param1Throwable) {
            }
        });

        this.listview = (RecyclerView) findViewById(R.id.recycler);
        listview.setLayoutManager(new LinearLayoutManager(this));
        listview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        this.txvResponse = (TextView) findViewById(R.id.txvResponse);
        this.txvStatus = (TextView) findViewById(R.id.txvStatus);
        ArrayList<String> arrayList = new ArrayList();
        arrayList.add("get Device Info");
        arrayList.add("get Error Log");
        arrayList.add("get Resistance Level Range");
        arrayList.add("get Resistance Level");
        arrayList.add("get Workout Control State");
        arrayList.add("Stop Workout");
        arrayList.add("Start Workout");
        arrayList.add("Increase Resistance Level");
        arrayList.add("Decrease Resistance Level");
        arrayList.add("RPM Ratio 1");
        arrayList.add("RPM Ratio 4.4");
        arrayList.add("Resistance Test");
        arrayList.add("get FTMP Valid Date");
        arrayList.add("Sync Time");
        arrayList.add("Get Sleep Setting");
        arrayList.add("Set Sleep Setting: off/0");
        arrayList.add("Set Sleep Setting: on/1");
        this.listview.setVisibility(View.INVISIBLE);
        adapter = new RecyclerAdapter(arrayList);
        listview.setAdapter(adapter);
        displayValues();
        adapter.setListener(new RecyclerAdapter.ItemClickListener() {
            @Override
            public void click(int position) {
                switch (position) {
                    default:
                        return;
                    case 16:
                        BLEManager.getInstance().getPeripheral().setSleepSetting(1);
                        break;
                    case 15:
                        BLEManager.getInstance().getPeripheral().setSleepSetting(0);
                        break;
                    case 14:
                        BLEManager.getInstance().getPeripheral().getSleepSetting();
                        break;
                    case 13:
                        long param1Long = System.currentTimeMillis();
                        BLEManager.getInstance().getPeripheral().syncTime(new Date(), new Date(param1Long + 2592000000L));
                        break;
                    case 12:
                        BLEManager.getInstance().getPeripheral().getFTMPValidDate();
                        break;
                    case 11:
                        ControlPaneActivity.this.startResistanceTest();
                        break;
                    case 10:
                        Workout.setRpmRatio(4.4D);
                        break;
                    case 9:
                        Workout.setRpmRatio(1.0D);
                        break;
                    case 8:
                        BLEManager.getInstance().getPeripheral().setResistanceLevel(ControlPaneActivity.this.level - 1);
                        break;
                    case 7:
                        BLEManager.getInstance().getPeripheral().setResistanceLevel(ControlPaneActivity.this.level + 1);
                        break;
                    case 6:
                        BLEManager.getInstance().getPeripheral().startWorkout();
                        break;
                    case 5:
                        BLEManager.getInstance().getPeripheral().stopWorkout();
                        break;
                    case 4:
                        BLEManager.getInstance().getPeripheral().getWorkoutControlState();
                        break;
                    case 3:
                        BLEManager.getInstance().getPeripheral().getResistanceLevel();
                        break;
                    case 2:
                        BLEManager.getInstance().getPeripheral().getResistanceLevelRange();
                        break;
                    case 1:
                        BLEManager.getInstance().getPeripheral().getErrorLog();
                        break;
                    case 0:
                        BLEManager.getInstance().getPeripheral().getDeviceInfo();
                        break;
                }
            }
        });

        if (BLEManager.getInstance().getPeripheral() != null) {
            if (BLEManager.getInstance().getInitialized())
                showControlPane();
            BLEManager.getInstance().registerListener(this);
            BLEManager.getInstance().getPeripheral().setListener(this);
            BLEManager.getInstance().getPeripheral().setLogging(true);
        }

        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        registerReceiver(mUsbReceiver, filter);
        if ( SharedPreUtils.getInstance(this).getBoolean(AppUtil.IS_TEST,false))
            timer = new InactivityTimer(this);
    }

    protected void onDestroy() {

        try {
            BLEManager.getInstance().unregisterListener(this);
            if (mUsbReceiver != null) {
                unregisterReceiver(mUsbReceiver);
            }
            if (mUsbAccessoryManager != null) {
                mUsbAccessoryManager.destoryAccessory(true);
            }
            mUsbAccessoryManager = null;
            Process.killProcess(Process.myPid());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        if (timer != null)
            timer.shutdown();
        super.onDestroy();
    }

    public void onGetControlStateResponse(int paramInt) {
        if (paramInt == 1) {
            setResponseMessage("Control State: Start");
        } else if (paramInt == 0) {
            setResponseMessage("Control State: Stop");
        } else if (paramInt == 2) {
            setResponseMessage("Control State: Pause");
        } else {
            setResponseMessage(String.format("Control State: %x", new Object[]{Integer.valueOf(paramInt)}));
        }
    }

    public void onGetDeviceInfoResponse(int paramInt, final String hwVersion,
                                        final String fwVersion) {
        runOnUiThread(new Runnable() {
            public void run() {
                ControlPaneActivity.this.txvResponse.setText(String.format("HW Version:%s\nFW Version:%s", new Object[]{hwVersion, fwVersion}));
                if (Workout.sharedInstance().isTreadmill())
                    ControlPaneActivity.this.showTreadmillCommands();
            }
        });
    }

    public void onGetErrorLogResponse(byte[] paramArrayOfbyte) {
        setResponseMessage(String.format("Error Codes: %s", new Object[]{ADConverter.byteArrayToHexString(paramArrayOfbyte)}));
    }

    public void onGetFTMPValidDateResponse(Date paramDate) {
        setResponseMessage(String.format("FTMP Valid Date: %s", new Object[]{paramDate.toGMTString()}));
    }

    public void onGetFanSpeedLevelResponse(int paramInt) {
        setResponseMessage(String.format("Current Fan Speed Level: %d", new Object[]{Integer.valueOf(paramInt)}));
    }

    public void onGetFanSpeedLimitResponse(int paramInt) {
        setResponseMessage(String.format("Max Fan Speed Level: %d", new Object[]{Integer.valueOf(paramInt)}));
    }

    public void onGetInclineResponse(int paramInt) {
        setResponseMessage(String.format("Incline: %d", new Object[]{Integer.valueOf(paramInt)}));
        this.incline = paramInt;
    }

    public void onGetLimitsResponse(int paramInt1, int paramInt2, double paramDouble1,
                                    double paramDouble2, int paramInt3) {
        setResponseMessage(String.format("Max Incline:%d\nMin Incline:%d\nMax Speed:%.1f\nMin Speed:%.1f", new Object[]{Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Double.valueOf(paramDouble1), Double.valueOf(paramDouble2)}));
    }

    public void onGetResistanceLevelRangeResponse(int paramInt1, int paramInt2) {
        setResponseMessage(String.format("Max Level:%d\nMin Level:%d", new Object[]{Integer.valueOf(paramInt1), Integer.valueOf(paramInt2)}));
    }

    public void onGetResistanceLevelResponse(int paramInt) {
        setResponseMessage(String.format("Resistance Level: %d", new Object[]{Integer.valueOf(paramInt)}));
    }

    public void onGetSleepSettingResponse(int paramInt) {
        setResponseMessage(String.format("Sleep Setting: %d", new Object[]{Integer.valueOf(paramInt)}));
    }

    public void onGetSpeedResponse(double paramDouble) {
        setResponseMessage(String.format("Speed: %.1f", new Object[]{Double.valueOf(paramDouble)}));
        this.speed = paramDouble;
    }

    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onReadErrorOccur() {
        finish();
    }

    protected void onResume() {
        super.onResume();
        resumeAccessory();
    }


    protected void onStop() {
        super.onStop();
    }

    public void peripheralInitialized() {
        showControlPane();
    }

    public void resistanceLevelChanged(int paramInt) {
        this.level = paramInt;
        displayValues();
    }

    private void resumeAccessory() {

        if (mUsbAccessoryManager != null)
            return;
        UsbAccessory[] accessories = mUsbManager.getAccessoryList();
        if (accessories != null) {
            Toast.makeText(this, "Accessory Attached", Toast.LENGTH_SHORT).show();
        } else {
            // return 2 for accessory detached case
            //Log.e(">>@@","ResumeAccessory RETURN 2 (accessories == null)");
            return;
        }
        UsbAccessory accessory = (accessories == null ? null : accessories[0]);
        if (accessory != null) {
            if (-1 == accessory.toString().indexOf(ManufacturerString)) {
                Toast.makeText(this, "Manufacturer is not matched!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (-1 == accessory.toString().indexOf(VersionString)) {
                Toast.makeText(this, "Version is not matched!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (accessory.toString().indexOf(ModelString1) > 0) {
                Toast.makeText(this, "UART device is attached.", Toast.LENGTH_SHORT).show();
            } else if (accessory.toString().indexOf(ModelString2) > 0) {
                Toast.makeText(this, "UART device is attached.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Model is not matched!", Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(this, "Manufacturer, Model & Version are matched!", Toast.LENGTH_SHORT).show();
            if (mUsbManager.hasPermission(accessory)) {
                mPermissionRequestPending = true;
                mUsbAccessory = accessory;
                mUsbAccessoryManager = UsbAccessoryManager.createInstance(mUsbManager, mUsbAccessory);
                mUsbAccessoryManager.setListener(this);
                mUsbAccessoryManager.openAccessory();
                otgBikeMode();
            } else {
                synchronized (mUsbReceiver) {
                    if (!mPermissionRequestPending) {
                        Toast.makeText(this, "Request USB Permission", Toast.LENGTH_SHORT).show();
                        mUsbManager.requestPermission(accessory,
                                mPermissionIntent);
                        mPermissionRequestPending = true;
                    }
                }
            }
        }
    }

    public void speedChanged(double paramDouble) {
        this.speed = paramDouble;
        displayValues();
    }

    public void userKeyPressed(int paramInt1, int paramInt2) {
        setResponseMessage(String.format("User Key: %d, Value: %d", new Object[]{Integer.valueOf(paramInt1), Integer.valueOf(paramInt2)}));
    }

    public void workoutStatusChanged(int paramInt1, double paramDouble, int paramInt2,
                                     int paramInt3) {
        this.time = paramInt1;
        this.distance = paramDouble;
        this.calories = paramInt2;
        this.hr = paramInt3;
        displayValues();
    }

    public void workoutStatusChanged(int paramInt1, int paramInt2, int paramInt3,
                                     int paramInt4, double paramDouble1, double paramDouble2, double paramDouble3,
                                     double paramDouble4) {
        this.time = paramInt1;
        this.count = paramInt2;
        this.rpm = paramInt3;
        this.hr = paramInt4;
        this.speed = paramDouble1;
        this.distance = paramDouble2;
        this.calories = paramDouble3;
        this.watt = paramDouble4;
        displayValues();
    }

    public void workoutStatusChanged(int paramInt1, int paramInt2, int paramInt3,
                                     int paramInt4, int paramInt5, double paramDouble1, double paramDouble2, int paramInt6,
                                     double paramDouble3, double paramDouble4) {
        this.time = paramInt1;
        this.count = paramInt3;
        this.rpm = paramInt4;
        this.hr = paramInt5;
        this.speed = paramDouble1;
        this.distance = paramDouble2;
        this.calories = paramDouble3;
        this.watt = paramDouble4;
        this.time500 = paramInt6;
        this.direction = paramInt2;
        displayValues();
    }

}


/* Location:              /home/sakura/work/test/new/EchelonSDK_1.1.24_zip/classes-dex2jar.jar!/changyow/santo/ControlPaneActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */