package com.tes.echelonsdk.util;

import android.app.Activity;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class InactivityTimer {
    private static int INACTIVITY_DELAY_SECONDS = 30;
    private final ScheduledExecutorService inactivityTimer =
            Executors.newSingleThreadScheduledExecutor(new DaemonThreadFactory());
    private Activity activity;
    private ScheduledFuture<?> inactivityFuture = null;

    public InactivityTimer(Activity activity) {
        this.activity = activity;
        String time = SharedPreUtils.getInstance(activity).getString(AppUtil.TIME);
        double timeDouble = 0f;
        try {
            timeDouble = Double.parseDouble(time);
        } catch (Exception e) {
        }
        INACTIVITY_DELAY_SECONDS = (int) timeDouble * 60;
        if (INACTIVITY_DELAY_SECONDS <= 0) {
            INACTIVITY_DELAY_SECONDS = 30;
        }

        onActivity();

    }

    public void onActivity() {
        cancel();
        inactivityFuture = inactivityTimer.schedule(new FinishListener(activity), INACTIVITY_DELAY_SECONDS, TimeUnit.SECONDS);

    }

    private void cancel() {
        if (inactivityFuture != null) {
            inactivityFuture.cancel(true);
            inactivityFuture = null;
        }
    }

    public void shutdown() {
        cancel();
        inactivityTimer.shutdown();
    }

    public class DaemonThreadFactory implements ThreadFactory {
        @Override
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setDaemon(true);
            return thread;
        }
    }
}
