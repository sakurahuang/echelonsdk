package com.tes.echelonsdk.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.tes.echelonsdk.MainActivity;

import java.util.Calendar;

public class FinishListener implements Runnable {

    private Activity activityToFinish;

    public FinishListener(Activity activityToFinish) {
        this.activityToFinish = activityToFinish;
    }

    @Override
    public void run() {
        Intent intent = activityToFinish.getPackageManager()
                .getLaunchIntentForPackage(activityToFinish.getApplication().getPackageName());
        PendingIntent restartIntent = PendingIntent.getActivity(activityToFinish.getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager mgr = (AlarmManager) activityToFinish.getSystemService(Context.ALARM_SERVICE);
        String time = SharedPreUtils.getInstance(activityToFinish).getString(AppUtil.START_TIME);
        double timeDouble = 0f;
        try {
            timeDouble = Double.parseDouble(time);
        } catch (Exception e) {
        }
        int addTime = (int) (timeDouble * 60*1000);
        if (addTime <= 0) {
            addTime = 5000;
        }
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + addTime, restartIntent); // 5秒钟后重启应用
        if (AppUtil.CONNECT_STATUA == 2) {
            int success = SharedPreUtils.getInstance(activityToFinish).getInt(AppUtil.SUCCESS, 0);
            SharedPreUtils.getInstance(activityToFinish).putInt(AppUtil.SUCCESS, success + 1);
        } else if (AppUtil.CONNECT_STATUA == 0) {
            int fail = SharedPreUtils.getInstance(activityToFinish).getInt(AppUtil.FAIL, 0);
            SharedPreUtils.getInstance(activityToFinish).putInt(AppUtil.FAIL, fail + 1);
        }
        System.out.println(Calendar.getInstance().getTime() + "BLEManager destory");
        System.exit(0);
    }
}
