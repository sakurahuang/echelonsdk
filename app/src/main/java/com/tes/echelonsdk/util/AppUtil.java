package com.tes.echelonsdk.util;

import android.os.Build;

public class AppUtil {


    public static final String IS_TEST = "is_test";
    public static final String LOG_PATH = "log_path";
    public static final String SUCCESS = "success";
    public static final String FAIL = "fail";
    public static final String TIME = "time";
    public static final String START_TIME = "start_time";

    public static int CONNECT_STATUA;

    /**
     * 是否是瑞讯的板子
     *
     * @return
     */
    public static boolean isMTB() {
        return "rk3288_mtb813".contains(Build.MODEL) || "rk3288_mtb818".contains(Build.MODEL);
    }
}
